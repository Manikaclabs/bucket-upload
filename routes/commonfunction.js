
var nodemailer = require('nodemailer');
var config = require('config');
var AWSSettings = config.get('AWSSettings');
/*
 * ------------------------------------------------------
 * Check if manadatory fields are not filled
 * INPUT : array of field names which need to be mandatory
 * OUTPUT : Error if mandatory fields not filled
 * ------------------------------------------------------
 */
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '') {
            return 1;
            break;
        }
        else if (arr[i] == undefined) {
            return 1;
            break;
        }
        else if (arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * check Email Availability
 * INPUT : email
 * OUTPUT : email available or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */

exports.checkEmailAvailability=function (res, email, callback) {

    var sql = "SELECT `user_id` FROM `users` WHERE `email`=? limit 1";
    var values = [email];
    connection.query(sql, values, function (err, userResponse) {

        if (userResponse.length) {

            sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_EXISTS, res);
        }
        else {
            callback();
        }
    });
};

/*
 * -----------------------------------------------------------------------------
 * Encryption code
 * INPUT : string
 * OUTPUT : crypted string
 * -----------------------------------------------------------------------------
 */
exports.encrypt = function (text) {

    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
};
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * check Email validity
 * INPUT : email
 * OUTPUT : email registered or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.checkEmail=function (res, email, callback) {
    var sql = "SELECT `user_id` FROM `users` WHERE `email`=? limit 1";
    var values = [email];
    connection.query(sql, [email], function (err,userResponse) {
        if (userResponse.length) {
            callback(null);
        }
        else {
            
            sendResponse.unregisteredEmailMsg( constant.responseMessage.EMAIL_NOT_REGISTERED, res);
            
        }
    });
};

/*
 * ------------------------------------------------------
 * Authenticate a user through Access token and return extra data
 * Input:Access token{Optional Extra data}
 * Output: User_id Or Json error{Optional Extra data}
 * ------------------------------------------------------
 */
exports.accessTokenCheck= function (accesstoken,res ,callback) {
    console.log(accesstoken);
    var sql = "SELECT `user_id`";
    sql += " FROM `users`";
    sql += " WHERE `access_token`=? LIMIT 1";
    var values = [accesstoken];
    connection.query(sql, values, function (err,result) {
        if (err) {
            sendResponse.invalidAccessTokenError(res);
        } else {
            //console.log(result);
            return callback(result[0].user_id);
        }
    });

};
/*
 * ------------------------------------------------------
 * Send Email to user for one time password
 * Input:email
 * Output: email Or Json error{Optional Extra data}
 * ------------------------------------------------------
 */
exports.sendEmail=function(message,subject,receiverId,callback){

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user:config.get('emailSettings.user'),
        pass:config.get('emailSettings.pass')
    }
});
transporter.sendMail({
    from: config.get('emailSettings.user'),
    to: receiverId,
    subject: subject,
    text: message
},function(err,result)
{
if(err)
{
    console.log(err);
    return(callback(0));
}
else
{
    return(callback(1));
}

});
};


/*
 * ------------------------------------------------------
 * Upload user image to S3 bucket
 * Input:user_image
 * Output: success message or JSON error
 * ------------------------------------------------------
 */

exports.uploadImageToS3Bucket = function(file, folder, callback)
{   
    var fs = require('fs');
    var AWS = require('aws-sdk');

    var filename = file.name; // actual filename of file
    var path = file.path; //will be put into a temp directory
    var mimeType = file.type;

    fs.readFile(path, function(error, file_buffer) {
        if (error)
        {   
            return callback(0);
        }

        filename = file.name;
        AWS.config.update({accessKeyId: AWSSettings.awsAccessKey, secretAccessKey: AWSSettings.awsSecretKey});
        var s3bucket = new AWS.S3();
        var params = {Bucket: AWSSettings.awsBucket, Key: folder + '/' + filename, Body: file_buffer, ACL: 'public-read', ContentType: 'image/jpeg'};

        s3bucket.putObject(params, function(err, data) {
            if (err)
            {  
                return callback(0);
            }
            else {
                return callback(filename);
            }
        });
    });
};