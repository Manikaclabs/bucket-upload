var mysql = require('mysql');
var config = require('config');
var dbConfig = config.get('DatabaseSettings');
connection = mysql.createConnection({
host: dbConfig.host,
user: dbConfig.user,
database: dbConfig.database
});
connection.connect();
