
var router = express.Router();
var md5 = require('MD5');
var async = require('async');
var passwordGenerate = require('password-generator');
/*
 * --------------------------------------------------------------------------
 * sign_up
 * INPUT : firstName, lastName, email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/sign_up', function (req, res, next)
{
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var manValues = [firstName, lastName, email, password];
    async.waterfall([
        function (callback) {
            func.checkBlank(res, manValues, callback);
        },
        function (callback) {
            func.checkEmailAvailability(res, email, callback);
        }],
            function (resultCallback) {
                func.uploadImageToS3Bucket(req.files.user_image, 'user_image', function (result_user_pic) {
                    if (result_user_pic === 0) {
                        sendResponse.somethingWentWrongError(res);

                    }
                    else {
                        var path = "http://internsclabs/s3.amazonaws.com/user_image/" + result_user_pic;
                        var loginTime = new Date();
                        var accessToken = func.encrypt(email + loginTime);
                        var encryptPassword = md5(password);
                        var sql = "INSERT into users(email,password,access_token,last_login,first_name,last_name,user_image)values(?,?,?,?,?,?,?)";
                        var values = [email, encryptPassword, accessToken, loginTime, firstName, lastName, path];

                        connection.query(sql, values, function (err, userInsertResult) {
                            if (err) {

                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                var data = {
                                    access_token: accessToken,
                                    first_name: firstName,
                                    last_name: lastName
                                };
                                sendResponse.sendSuccessData(data, res);
                            }
                        });

                    }

                });
            });
});
/*
 * --------------------------------------------------------------------------
 * login_user
 * INPUT : email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/login_user', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var checkBlank = [email, password];
    async.waterfall(
            [function (callback) {
                    func.checkBlank(res, checkBlank, callback);
                }, function (callback) {
                    func.checkEmail(res, email, callback);
                }],
            function (resultCallback) {

                var loginTime = new Date();
                var accessToken = func.encrypt(email + loginTime);
                var loginStatus = 1;
                var sql = "UPDATE `users` SET `access_token`=?,`last_login`=?,`login_status`=? WHERE `email`=? LIMIT 1";
                var values = [accessToken, loginTime, loginStatus, email];

                connection.query(sql, values, function (err, userInsertResult) {
                    if (err) {
                        console.log(err);
                        res.send("ERR");
                    }
                    else {

                        sendResponse.sendSuccessData("LOGGED IN", res);
                    }

                });

            });
});
/*
 * --------------------------------------------------------------------------
 * forgot password
 * INPUT : email
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/forgot_password', function (req, res, next)
{
    var email = req.body.email;
    var mainValues = [email];
    func.checkBlank(res, mainValues, function (callback) {
        if (callback == null)
        {
            var sql = "SELECT user_id FROM users WHERE email=? LIMIT 1";
            connection.query(sql, [email], function (err, response1) {
                if (err)
                {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var id = [email];
                    var newPassword = passwordGenerate(7, false);
                    var encryptedPass = md5(newPassword);
                    var subject = "Password Reset";
                    var message = "This is a one time password.Please reset your password. Click on the given link to reset password";
                    message += encryptedPass;
                    func.sendEmail(message, subject, id, function (callback)
                    {
                        if (callback == 0)
                        {
                            sendResponse.sendErrorMessage(constant.responseMessage.email_sending_failed, res);
                        }
                        else
                        {
                            var sql1 = "UPDATE `users` SET `one_time_link`=?,`password_reset`=? WHERE `user_id`=? LIMIT 1";
                            connection.query(sql1, [encryptedPass, 0, response1[0].user_id], function (err, result) {
                                if (err)
                                {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else
                                {
                                    sendResponse.sendSuccessData(constant.responseMessage.email_sending, res);
                                }
                            });

                        }
                    });
                }
            });
        }
        else
        {
            sendResponse.unregisteredEmailMsg(constant.responseMessage.EMAIL_NOT_REGISTERED, res);
        }
    });
});
/*
 * --------------------------------------------------------------------------
 * Reset password
 * INPUT : one time link
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/reset_password', function (req, res, next) 
{
    var link = req.body.link;
    var newPassword = req.body.new_password;
    var parameters = [link, newPassword];
    func.checkBlank(res, parameters, function (callback) {
        if (callback === null)
        {
            var sql = "SELECT `user_id` FROM `users` WHERE `one_time_link`=? LIMIT 1";
            connection.query(sql, [link], function (err, results) {
                console.log(err);
                if (results == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {
                    var hash = md5(newPassword);
                    var sql2 = "UPDATE `users` SET `one_time_link`=? ,`password_reset`=?, `password`=? WHERE `user_id`=? LIMIT 1";
                    connection.query(sql2, ["",1, hash, results[0].user_id], function (err, result1) {
                        if (err)
                        {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else
                        {
                            sendResponse.successStatusMsg("Password reset successful", res);
                        }


                    });
                }
            });
        }
        else {
            sendResponse.somethingWentWrongError(res);
        }

    });

});
/*
 * --------------------------------------------------------------------------
 * logout
 * INPUT : email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/logout', function (req, res, next) {
    var accessToken = req.body.access_token;
    async.waterfall(
            [function (callback) {
                    func.checkBlank(res, accessToken, callback);
                },
                function (callback) {
                    func.accessTokenCheck(accessToken, res, callback);
                }],
            function (callback) {
                var loginStatus =0;
                var sql = "UPDATE users SET login_status=? WHERE user_id=? LIMIT 1";
                var values = [loginStatus, callback];
                connection.query(sql, values, function (err, userInsertResult) {
                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        sendResponse.logoutSuccess(constant.responseMessage.LOGOUT_SUCCESSFULL, res);
                    }
                });
            });
});
module.exports = router;